package httpRequest

import (
	"net/http"

	"gitlab.com/afey13/logistic-gocommon/errors"
)

type HttpRequestGatewayHelper interface {
	Request(r *http.Request, host string) (*http.Response, errors.CodedError)
}
