package cryptography

//go:generate mockgen -source=cryptography.go -destination=mock/cryptography_mock.go -package=mock

import (
	"gitlab.com/afey13/logistic-gocommon/errors"
)

type Cryptography interface {
	Encrypt(stringToEncrypt string) (string, errors.CodedError)
	Decrypt(encryptedString string) (string, errors.CodedError)
}
