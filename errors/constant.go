package errors

const (
	ErrorCodePlatformInvalid   int    = 101
	ErrorCodePlatformRequired  int    = 100
	MessagePlatformInvalid     string = "invalid platform"
	MessagePlatformRequired    string = "required platform"
	MessageKeyPlatformInvalid  string = "err.check.platform.invalid"
	MessageKeyPlatformRequired string = "err.check.platform.required"
)

var NewErrorHeaderPlatformInvalid = NewCodedError(ErrorCodePlatformInvalid, MessageKeyPlatformInvalid, MessagePlatformInvalid)
var NewErrorHeaderPlatformRequired = NewCodedError(ErrorCodePlatformRequired, MessageKeyPlatformRequired, MessagePlatformRequired)
