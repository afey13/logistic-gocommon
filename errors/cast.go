package errors

func AsCodedError(err error) CodedError {
	codedErr, ok := err.(CodedError)
	if !ok {
		return NewCodedError(ErrCodeUnknownError, UnknownErrMsgKey, err.Error())
	}

	return codedErr
}

func AsCodedFieldsError(err error) CodedFieldsError {
	codedErr, ok := err.(CodedFieldsError)
	if !ok {
		return NewCodedFieldsError(ErrCodeUnknownError, UnknownErrMsgKey, err.Error(), nil)
	}

	return codedErr
}
